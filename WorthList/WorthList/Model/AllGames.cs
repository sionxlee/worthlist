﻿using System;
using System.Collections.Generic;
using System.Text;
using QuickType;

namespace WorthList.Model
{
    class AllGames
    {
        public readonly static AllGames Default = new AllGames();

        public List<Game> gamelist { get; set; }
    }
}

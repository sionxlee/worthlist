﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var allgamelist = Allgamelist.FromJson(jsonString);

namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Allgamelist
    {
        [JsonProperty("applist")]
        public Gamelist Applist { get; set; }
    }

    public partial class Gamelist
    {
        [JsonProperty("apps")]
        public Game[] Apps { get; set; }
    }

    public partial class Game
    {
        [JsonProperty("appid")]
        public string gameID { get; set; }

        [JsonProperty("name")]
        public string gameName { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }
    }

    public partial class Allgamelist
    {
        public static Allgamelist FromJson(string json) => JsonConvert.DeserializeObject<Allgamelist>(json, QuickType.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Allgamelist self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

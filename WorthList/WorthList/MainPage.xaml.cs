﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickType;

using Xamarin.Forms;
using WorthList.Model;
using System.IO;
using System.Reflection;

namespace WorthList
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            
            

        }
        async void SignInClicked(object sender, EventArgs e)
        {
            //string sign= ((Button)sender).ClassId.ToString();
            //if(sign.Contains('1'))
            //{
            //    await Navigation.PushAsync(new Home(name.Text.ToString()));
            //}
            //else
            if (name.Text == null)
            {
                await DisplayAlert("Wrong", "Empty User Name", "Confirm");
            }
            else if (password.Text == null)
            {
                await DisplayAlert("Wrong", "Empty Password", "Confirm");
            }
            else
            {
                User.Default.Email = name.Text.ToString();
                AllGames.Default.gamelist = new List<Game>();
                TestList.Default.gamelist = new List<Game>()
                {
                    new Game()
                {
                    gameName ="Next Stop 2",
                    Price = "$6.99",
                    gameID="544440"},
                new Game()
                {
                    gameName ="Stop! Dictator.",
                    Price = "$1.99",
                    gameID="855100"},
                new Game()
                {
                    gameName ="Dino Dawn",
                    Price = "$4.99",
                    gameID="854160"},
                new Game()
                {
                    gameName ="Digit Daze",
                    Price = "$1.99",
                    gameID="854560"}
                };

                Assembly assembly;
                StreamReader streamReader;

                assembly = Assembly.GetExecutingAssembly();
                streamReader = new StreamReader(assembly.GetManifestResourceStream("WorthList.GamesAndPrices.txt"));

                string text = "";
                text = streamReader.ReadToEnd();

                var allgames = Allgamelist.FromJson(text);

                foreach(var game in allgames.Applist.Apps)
                {
                    AllGames.Default.gamelist.Add(game);
                }

                await Navigation.PushAsync(new Home());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using WorthList.Model;
using QuickType;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorthList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Edit : ContentPage
	{
        ObservableCollection<Game> gameListView;
        string email = "test@test.com";

        public Edit ()
		{
			InitializeComponent ();

            Checklistview.ItemsSource = AllGames.Default.gamelist;
        }

        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var game = (Game)menuItem.CommandParameter;
            gameListView.Remove(game);

            Checklistview.ItemsSource = gameListView;
        }

        async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Game itemTapped = (Game)listView.SelectedItem;
            bool userResponse = await DisplayAlert("Adding game", "Adding "+itemTapped.gameName+" into your list?", "Yes", "No");
            {
                if (userResponse)
                {
                    TestList.Default.gamelist.Add(itemTapped);
                }
            }
        }

        void HandleMoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var game = (Game)menuItem.CommandParameter;
            var uri = new Uri("http://store.steampowered.com/app/" + game.gameID + "/");
            Device.OpenUri(uri);
        }
        
        public void Searchtextchanged(object sender, TextChangedEventArgs e)
        {
            Checklistview.BeginRefresh();
            //searching part grab Info make a list
            if (string.IsNullOrWhiteSpace(e.NewTextValue))
                Checklistview.ItemsSource = null;
            else
                Checklistview.ItemsSource = AllGames.Default.gamelist.Where(i => i.gameName.CaseInsensitiveContains(e.NewTextValue));
            Checklistview.EndRefresh();
        }

        
    }
    public static class Extensions
    {
        public static bool CaseInsensitiveContains(this string text, string value, StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }
    }
}
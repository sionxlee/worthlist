﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using WorthList.Model;
using QuickType;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorthList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]


	public partial class Check : ContentPage
	{
        ObservableCollection<Game> gameListView;
        string email = "test@test.com";

        public Check ()
		{
			InitializeComponent ();
            checkList();

        }

        async void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            Game game = (Game)menuItem.CommandParameter;
            //remove from database and refresh or just remove 
            bool userResponse = await DisplayAlert("Adding game", "Adding " + game.gameName + " into your list?", "Yes", "No");
            {
                if (userResponse)
                {
                    TestList.Default.gamelist.Remove(game);
                }
            }
            checkList();

            Checklistview.ItemsSource = gameListView;
        }



        void HandleMoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var game = (Game)menuItem.CommandParameter;
            var uri = new Uri("http://store.steampowered.com/app/"+game.gameID+"/");
            Device.OpenUri(uri);
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            checkList();
            Checklistview.IsRefreshing = false;
        }
        public void checkList()
        {

            //grab game id from email
            gameListView = new ObservableCollection<Game>();
            Checklistview.ItemsSource = null;

            Checklistview.ItemsSource = TestList.Default.gamelist;
        }
    }
}